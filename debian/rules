#!/usr/bin/make -f

override_dh_install:
	dh_install
	# Not upstreamed
	chmod a+x debian/mediawiki/usr/share/mediawiki/maintenance/language/generate-collation-data

	# Now some tidying up is required
	chmod a-x debian/mediawiki/usr/share/mediawiki/vendor/pear/mail/tests/bug17178.phpt
	chmod a-x debian/mediawiki/usr/share/mediawiki/vendor/pear/mail/tests/bug17317.phpt
	# These were fixed upstream already: https://github.com/liuggio/statsd-php-client/pull/52
	chmod a-x debian/mediawiki/usr/share/mediawiki/vendor/liuggio/statsd-php-client/src/Liuggio/StatsdClient/Entity/StatsdData.php
	chmod a-x debian/mediawiki/usr/share/mediawiki/vendor/liuggio/statsd-php-client/tests/Liuggio/StatsdClient/StatsdDataFactoryTest.php
	chmod a-x debian/mediawiki/usr/share/mediawiki/vendor/liuggio/statsd-php-client/src/Liuggio/StatsdClient/Entity/StatsdDataInterface.php
	chmod a-x debian/mediawiki/usr/share/mediawiki/vendor/liuggio/statsd-php-client/tests/Liuggio/StatsdClient/Entity/StatsdDataTest.php

	chmod a-x debian/mediawiki/usr/share/mediawiki/vendor/wikimedia/less.php/lib/Less/Less.php.combine

	# Remove Scribunto binaries
	rm -rf debian/mediawiki/var/lib/mediawiki/extensions/Scribunto/includes/engines/LuaStandalone/binaries

	find debian/mediawiki/usr/share/mediawiki -maxdepth 1 -mindepth 1 | grep -v "\(LocalSettings.php\|debian-scripts\|images\|extensions\|config\)" | \
	while read i; do \
		dh_link "`echo "$$i" | sed -e s#debian/mediawiki/##`" \
		"`echo "$$i" | sed -e s#debian/mediawiki/usr/share/mediawiki/#var/lib/mediawiki/#`"; \
	done
	# Remove Makefiles
	find debian/mediawiki/ -iname makefile -exec rm {} \;
	# Remove CoC files
	find debian/mediawiki/ -iname CODE_OF_CONDUCT.md -exec rm {} \;
	# Remove extra tests
	find debian/mediawiki/ -path '*/tests' -prune -exec rm -rf {} \;
	# Move extensions
	mkdir -p debian/mediawiki/usr/share/doc/mediawiki
	mv debian/mediawiki/var/lib/mediawiki/extensions/README \
	    debian/mediawiki/usr/share/doc/mediawiki/README.extensions
	mv debian/mediawiki/var/lib/mediawiki/extensions \
	    debian/mediawiki/usr/share/mediawiki/extensions-core
	mkdir debian/mediawiki/var/lib/mediawiki/extensions
	coreextensions=$$(cd debian/mediawiki/usr/share/mediawiki/extensions-core; \
	    echo *); for coreextension in $$coreextensions; do \
		dh_link usr/share/mediawiki/extensions-core/"$$coreextension" \
		    var/lib/mediawiki/extensions/"$$coreextension"; \
	done
	# includes/libs is provided by mediawiki-classes
	rm -rf debian/mediawiki/usr/share/mediawiki/includes/libs
	# Remove mediawiki-classes README, already installed into /usr/share/doc
	rm debian/mediawiki-classes/usr/share/mediawiki/includes/libs/README
	rm debian/mediawiki-classes/usr/share/mediawiki/includes/libs/Message/README.md
	rm debian/mediawiki-classes/usr/share/mediawiki/includes/libs/ParamValidator/README.md
	rm debian/mediawiki-classes/usr/share/mediawiki/includes/libs/objectcache/README.md
	rm debian/mediawiki-classes/usr/share/mediawiki/includes/libs/APACHE-LICENSE-2.0.txt

override_dh_installsystemd:
	# Do not enable by default, the user needs to manually
	# configure MediaWiki first.
	dh_installsystemd --no-enable --name=mediawiki-jobrunner

%:
	dh $@ --with apache2
